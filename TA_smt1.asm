.model SMALL 
.code 
 ORG 100h 
tdata: jmp proses 
 lusername db 13,10,    'Nama Penumpang        : $' 
 lnomor db 13,10,       'Nomor Identitas       : $'
 ltgl db 13,10,         'Tanggal Keberangkatan : $'
 lkbr db 13,10,         'Stasiun Keberangkatan : $'
 ltj db 13,10,          'Stasiun Tujuan        : $'
 lka db 13,10,          'Kereta Api            : $'
 lkls db 13,10,         'Kelas Penumpang       : $'   
 
Sps DB 0DH,0AH,  " $"
Tiket DB 0DH,0AH,        "              Rincian Tiket  $"
Spasi DB 0DH,0AH,  " $"
Bo DB   0DH,0AH, " Kode Booking     : 50F444V$" 
Nama DB 0DH,0AH, " Nama             : Muhammad Adhitya P.$"
Ni DB   0DH,0AH, " Nomor Identitas  : 2100018481$"
Ka DB   0DH,0AH, " Kereta Api       : LODAYA/159$"
Tp DB   0DH,0AH, " Tipe Penumpang   : A$"
Ntd DB  0DH,0AH, " No Tempat Duduk  : PRE-5 18A$"
Ber DB  0DH,0AH, " Berangkat        : YOGYAKARTA(YK)$"
Jam DB  0DH,0AH, "                  : 15 JAN 2022 08:15$"
Tb DB   0DH,0AH, " Perkiraan Tiba   : BANDUNG(BD)$"
Dat DB   0DH,0AH,"                  : 15 JAN 2022 16:10$"
Spa DB 0DH,0AH,  " $"
Ket DB   0DH,0AH,"                 SUDAH VAKSIN$"
  
 vstring1 db 23,?,23 dup(?) 
 vstring2 db 23,?,23 dup(?) 
proses: 
 mov ah,09h 
 lea dx,lusername 
 int 21h 
 mov ah,0ah 
 lea dx,vstring1 
 int 21h 
 mov ah,09h 
 lea dx,lnomor 
 int 21h 
 mov ah,0ah 
 lea dx,vstring2 
 int 21h
 mov ah,09h 
 lea dx,ltgl 
 int 21h 
 mov ah,0ah 
 lea dx,vstring1 
 int 21h
 mov ah,09h 
 lea dx,lkbr 
 int 21h 
 mov ah,0ah 
 lea dx,vstring1 
 int 21h
 mov ah,09h 
 lea dx,ltj 
 int 21h 
 mov ah,0ah 
 lea dx,vstring1 
 int 21h
 mov ah,09h 
 lea dx,lka 
 int 21h 
 mov ah,0ah 
 lea dx,vstring1 
 int 21h
 mov ah,09h 
 lea dx,lkls 
 int 21h 
 mov ah,0ah 
 lea dx,vstring1 
 int 21h
  
 lea si,vstring1 
 lea di,vstring2 
 cld 
 mov cx,23 
 rep cmpsb
 
MOV AH,9H 
LEA DX,Sps
INT 21H
LEA DX,Sps
INT 21H
LEA DX,Tiket
INT 21H
LEA DX,Spasi
INT 21H
LEA DX,Bo
INT 21H
LEA DX,Nama 
INT 21H
LEA DX,Ni
INT 21H
LEA DX,Ka
INT 21H
LEA DX,Tp
INT 21H
LEA DX,Ntd
INT 21H
LEA DX,Ber
INT 21H
LEA DX,Jam
INT 21H
LEA DX,Tb
INT 21H
LEA DX,Dat
INT 21H
LEA DX,Spa
INT 21H
LEA DX,Ket 
 int 21h  
 int 20h 
end tdata
